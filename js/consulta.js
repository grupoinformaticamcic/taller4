//Sistema de información de inscripción de materias
//MCIC - Informática - Universidad Distrital Francisco José de Caldas

var preinscripcion = [];
var maestria = [];
var materia = [];

//Cargar datos
function cargardatos (nombre){
    a = localStorage.getItem(nombre)
    if (a == null)
    return []
    return JSON.parse(a)
 }

function cargar(){

    preinscripcion = cargardatos('inscripcion')
    maestria = cargardatos('maestria')
    materia = cargardatos('materia')
     }

cargar();

var tblBody = document.createElement("tbody");

function consultarPreinscripcion(){

    var codigoEstudiante = document.getElementById('codigo').value;
    var preinscripcionMaterias = consultarLocalStorage(codigoEstudiante)[0]['IdMateria'];
    var arrayLength = preinscripcionMaterias.length;
    var theTable = document.createElement('table');

    for (var i = 0, tr, td; i < arrayLength; i++) {
        tr = document.createElement('tr');
        td = document.createElement('td');
        td.appendChild(document.createTextNode(preinscripcionMaterias[i]));
        tr.appendChild(td);
        theTable.appendChild(tr);
    }

    document.getElementById('table').appendChild(theTable);

 
    return false;
}

function borrarConsulta(){
    var table = document.getElementById('table');
    while(table.hasChildNodes())
    {
       table.removeChild(table.firstChild);
    }
}

function consultarLocalStorage (codigo){
    cargar(); 
    var inscripcionmaterias = []
        for (i = 0; i<preinscripcion.length; i++){
            if (preinscripcion[i].codigo == codigo)
            inscripcionmaterias.push({"codigo" : preinscripcion[i].codigo,
                                      "nombre" : preinscripcion[i].nombre,
                                      "apellido" : preinscripcion[i].apellido,
                                      "IdMaestria" : preinscripcion[i].IdMaestria,
                                      "IdMateria" : preinscripcion[i].Materias}); 
        }
             return inscripcionmaterias;
    }