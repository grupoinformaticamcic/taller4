//Sistema de información de inscripción de materias
//MCIC - Informática - Universidad Distrital Francisco José de Caldas

//Maestrias por enfásis
var maestria = [{"codigo" : "11752801","nombre" : "Geomática"},
                {"codigo" : "11752802","nombre" : "Teleinformática"},
                {"codigo" : "11752803","nombre" : "Ingeniería de software"}]

                localStorage.setItem('maestria', JSON.stringify(maestria))

//Consulta maestria por código del enfásis
function consultarMaestria (codigo){

    for (i = 0; i<maestria.length; i++){
   
        if (maestria[i].codigo == codigo)
        return maestria[i];   
    }
        return null;
}
//Consulta todas las maestrías creadas en el sistema
function consultarMaestrias (){
cargar();
    for (i = 0; i<maestria.length; i++){
   
            return maestria;   
    }
}
//Materias
var materia = [{"codigo" : "11752801","codigom" : "01","nombre" : "Seminario de Investigación"},
               {"codigo" : "11752801","codigom" : "02","nombre" : "Herramientas Matemáticas para el Manejo de la Información - Software"},
               {"codigo" : "11752801","codigom" : "03","nombre" : "Informática"},
               {"codigo" : "11752801","codigom" : "04","nombre" : "Bases de Datos Espaciales"},
               {"codigo" : "11752801","codigom" : "05","nombre" : "Sistemas de Posicionamiento Geodésico"},
               {"codigo" : "11752801","codigom" : "06","nombre" : "Método Avanzado de Análisi de Imágenes"},
               {"codigo" : "11752801","codigom" : "07","nombre" : "Análisis Espacial"},
               {"codigo" : "11752801","codigom" : "08","nombre" : "Servicios Geográficos WEB"},
               {"codigo" : "11752801","codigom" : "09","nombre" : "Big Data"},
               {"codigo" : "11752801","codigom" : "10","nombre" : "Mineria de Datos"},
               {"codigo" : "11752801","codigom" : "11","nombre" : "BlockChain"},
               {"codigo" : "11752801","codigom" : "12","nombre" : "Inteligencia Computacional"},
               {"codigo" : "11752801","codigom" : "13","nombre" : "Computación Paralela"},
               {"codigo" : "11752802","codigom" : "01","nombre" : "Seminario de Investigación"},
               {"codigo" : "11752802","codigom" : "02","nombre" : "Herramientas Matemáticas para el Manejo de la Información - Software"},
               {"codigo" : "11752802","codigom" : "03","nombre" : "Informática"},
               {"codigo" : "11752802","codigom" : "03","nombre" : "Informática"},
               {"codigo" : "11752802","codigom" : "04","nombre" : "Procesos Estocásticos"},
               {"codigo" : "11752802","codigom" : "05","nombre" : "Modelado y Simulación de Redes"},
               {"codigo" : "11752802","codigom" : "06","nombre" : "Aplicaciones de Internet Sobre a Nube"},
               {"codigo" : "11752802","codigom" : "07","nombre" : "Comunicaciones"},
               {"codigo" : "11752802","codigom" : "08","nombre" : "Redes"},
               {"codigo" : "11752802","codigom" : "09","nombre" : "Big Data"},
               {"codigo" : "11752802","codigom" : "10","nombre" : "Mineria de Datos"},
               {"codigo" : "11752802","codigom" : "11","nombre" : "BlockChain"},
               {"codigo" : "11752802","codigom" : "12","nombre" : "Inteligencia Computacional"},
               {"codigo" : "11752802","codigom" : "13","nombre" : "Computación Paralela"},
               {"codigo" : "11752803","codigom" : "01","nombre" : "Seminario de Investigación"},
               {"codigo" : "11752803","codigom" : "02","nombre" : "Herramientas Matemáticas para el Manejo de la Información - Software"},
               {"codigo" : "11752803","codigom" : "03","nombre" : "Informática"},
               {"codigo" : "11752803","codigom" : "04","nombre" : "Ingeniería de Software II"},
               {"codigo" : "11752803","codigom" : "05","nombre" : "Bases de Datos"},
               {"codigo" : "11752803","codigom" : "06","nombre" : "Patrones y Arquitectura de Software"},
               {"codigo" : "11752803","codigom" : "07","nombre" : "Tendencias en Ingeniería de Sofware"},
               {"codigo" : "11752803","codigom" : "08","nombre" : "Big Data"},
               {"codigo" : "11752803","codigom" : "09","nombre" : "Mineria de Datos"},
               {"codigo" : "11752803","codigom" : "10","nombre" : "BlockChain"},
               {"codigo" : "11752803","codigom" : "11","nombre" : "Inteligencia Computacional"},
               {"codigo" : "11752803","codigom" : "12","nombre" : "Computación Paralela"}]
             
               localStorage.setItem('materia',JSON.stringify(materia))

               //Consultar materias por código
function consultarMateria (codigo){

    for (i = 0; i<materia.length; i++){
   
        if (materia[i].codigo == codigo)
        return materia[i];   
    }
        return null;
}
//Consultar todas las materias
function consultarMaterias (codigo){
cargar();
    var materias = []
    for (i = 0; i<materia.length; i++){
        if (materia[i].codigo == codigo)
        materias.push({"codigo": materia[i].codigo,
                       "codigom": materia[i].codigom,
                       "nombrem":materia[i].nombre}); 
    }
    return materias;
 }

 var selectMaestria = document.getElementById("seleccionEnfasis");

 for(var i = 0; i < maestria.length; i++) {
    var opt = "["+maestria[i]["codigo"]+"] "+maestria[i]["nombre"];
    var el = document.createElement("option");
    el.textContent = opt;
    el.value = opt;
    selectMaestria.appendChild(el);
}

 var selectAsignaturas = document.getElementById("seleccionAsignaturas");
 
 function cambioMaestria(){

    var length = selectAsignaturas.options.length;
    for (i = length-1; i >= 0; i--) {
        selectAsignaturas.options[i] = null;
    }

    var enf = document.getElementById("seleccionEnfasis").value;
    var codenf = enf.substring(1,9);
    var matenf = consultarMaterias(codenf);

    for(var i = 0; i < matenf.length; i++) {
        var opt = "["+matenf[i]["codigo"]+"] "+"["+matenf[i]["codigom"]+"] "+matenf[i]["nombrem"];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        selectAsignaturas.appendChild(el);
    }
 }

function preinscribir(){
    var codigoEstudiante = document.getElementById("codigo").value;
    var nombresEstudiante = document.getElementById("nombres").value;
    var apellidosEstudiante = document.getElementById("apellidos").value;
    var enfasisM = document.getElementById("seleccionEnfasis").value;
    var Asignaturas = getSelectValues(document.getElementById("seleccionAsignaturas"));

    agregarestudiante(codigoEstudiante,nombresEstudiante,apellidosEstudiante,enfasisM,Asignaturas);

return false;
}

function getSelectValues(select) {
    var result = [];
    var options = select && select.options;
    var opt;
  
    for (var i=0, iLen=options.length; i<iLen; i++) {
      opt = options[i];
  
      if (opt.selected) {
        result.push(opt.value || opt.text);
      }
    }
    return result;
  }

var estudiante = []
// Retorna true si el elemnto es insertado, false si ya preinscribio la materia seleccionada
function agregarestudiante (codigo,nombre,apellido,IdMaestria,IdMateria){
cargar ();
    for (i = 0; i<estudiante.length; i++){
   
        if ((estudiante[i].codigo == codigo && estudiante[i].IdMaestria == IdMaestria && estudiante[i].IdMateria == IdMateria)
        ||(estudiante[i].codigo == codigo && estudiante[i].IdMaestria != IdMaestria))
        return false;
    }
    estudiante.push({"codigo" : codigo,"nombre" : nombre,"apellido" : apellido,"IdMaestria" : IdMaestria, "Materias" : IdMateria});
    guardardatos(); 
    alert("Preinscripción realizada por "+ nombre + " "+apellido)   
    return true;
}
//Salva los datos en el local storage
function guardardatos (){
localStorage.setItem('inscripcion', JSON.stringify(estudiante))
localStorage.setItem('maestria', JSON.stringify(maestria))
localStorage.setItem('materia',JSON.stringify(materia))
}
//Carga los datos del local storage
function cargardatos (nombre){
    a = localStorage.getItem(nombre)
    if (a == null)
    return []
    return JSON.parse(a)
 }

 function cargar(){

estudiante = cargardatos('inscripcion')
maestria = cargardatos('maestria')
materia = cargardatos('materia')
 }
//Consulta estudiantes por código
function consultarestudiante (codigo){

    for (i = 0; i<estudiante.length; i++){
   
        if (estudiante[i].codigo == codigo)
        return estudiante[i];   
    }
        return null;
}
//Consulta todos los estudiantes
function consultarEstudiantes (codigo){
    cargar(); 
    var inscripcionmaterias = []
        for (i = 0; i<estudiante.length; i++){
            if (estudiante[i].codigo == codigo)
            inscripcionmaterias.push({"codigo" : estudiante[i].codigo,"nombre" : estudiante[i].nombre,"apellido" : estudiante[i].apellido,"IdMaestria" : estudiante[i].IdMaestria,"IdMateria" : estudiante[i].IdMateria}); 
        }
        return inscripcionmaterias;
    }